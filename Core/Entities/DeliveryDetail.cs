﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Entities
{
    public class DeliveryDetail
    {
        [Key]
        public int DeleveryId { get; set; }
        public int SellId { get; set; }
        public int DeleveryManId { get; set; }
        public DateTime BookReceivedDate { get; set; }
        public Nullable<DateTime> BookDeleverdDate { get; set; }
    }
}
