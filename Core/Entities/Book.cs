﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Entities
{
    public class Book
    {
        [Key]
        public int BookId { get; set; }
        public int UserId { get; set; }
        public string BookTitle { get; set; }
        public string BookAuthor { get; set; }
        public int BookEdition { get; set; }
        public int UsedMonths { get; set; }
        public int Point { get; set; }
        public int Status { get; set; }
    }
}
