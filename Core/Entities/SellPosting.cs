﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Entities
{
    public class SellPosting
    {
        [Key]
        public int SellId { get; set; }
        public int BuyerId { get; set; }
        public int SellerId { get; set; }
        public int BookId { get; set; }
        public int Status { get; set; }
    }
}
