﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Entities
{
    public class RequestedBook
    {
        [Key]
        public int RequestId { get; set; }
        public int UserId { get; set; }
        public string BookTitle { get; set; }
        public string BookAuthor { get; set; }
        public int BookEdition { get; set; }
    }
}
