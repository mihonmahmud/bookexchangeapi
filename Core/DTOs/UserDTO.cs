﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTOs
{
    public class UserDTO
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int Points { get; set; }
        public int TotalUpload { get; set; }
        public int TotalBuy { get; set; }
        public int TotalSell { get; set; }
        public List<Links> Links = new List<Links>();
    }
}
