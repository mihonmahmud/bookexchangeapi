﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTOs
{
    public class Links
    {
        public string HRef { get; set; }
        public string Method { get; set; }
        public string Rel { get; set; }
    }
}
