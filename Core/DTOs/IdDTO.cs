﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTOs
{
    public class IdDTO
    {
        public int[] Ids { get; set; }
    }
}
