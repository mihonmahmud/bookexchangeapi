﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTOs
{
    public class TopTenDTO
    {
        public string Email { get; set; }
        public int Count { get; set; }
    }
}
