﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTOs
{
    public class DeliveryManDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public double Salary { get; set; }
        public int Count { get; set; }
    }
}
