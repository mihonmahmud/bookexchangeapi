﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTOs
{
    public class UserDataDTO
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
