﻿using Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces
{
    public interface ISellPostingRepository
    {
        IEnumerable<DeliveryStatusDTO> DeliveryStatus();
        StatusCountDTO StatusCount();
        IEnumerable<DeliveryStatusDTO> PendingList();
    }
}
