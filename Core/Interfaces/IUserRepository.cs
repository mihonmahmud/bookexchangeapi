﻿using Core.DTOs;
using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
        UserDTO User(int id);
        IList<UserDTO> Users();
        bool IsUserExist(string email);
        int GetPoint(int id);
        void PutPoint(int id, int point);
        void DeleteByEmail(string mail);
        UserDTO GetByEmail(string mail);
        int TotalUsers();
        int GetStatus(string mail);
        IEnumerable<TopTenDTO> TopTen(string search);
        IEnumerable<DeliveryDetail> GetUserDeleveries(int userId);
        IEnumerable<SellPosting> GetbyUserId(int userId);
        IEnumerable<Book> GetUserBooks(int user);
        IEnumerable<Book> GetUserAcceptedBooks(int user);
    }
}
