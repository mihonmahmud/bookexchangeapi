﻿using Core.DTOs;
using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces
{
    public interface IDeliveryManRepository : IGenericRepository<DeliveryMan>
    {
        int TotalDeliveryMan();
        void AddDeliveryMan(DeliveryMan dm);
        IEnumerable<DeliveryManDTO> AllMen();
        DeliveryManDTO GetMan(int id);
        void DeleteByEmail(string mail);
        bool IsEmailExist(string mail);
        int TotalDeliveries();
        IEnumerable<DeliveryDetail> GetDeliveriesByID(int dmanID);
    }
}
