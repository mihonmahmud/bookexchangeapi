﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces
{
    public interface IUserUOW
    {
        IGenericRepository<Login> login { get; }
        IUserRepository user { get; }
        int Complete();
    }
}
