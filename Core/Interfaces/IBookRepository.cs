﻿using Core.DTOs;
using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        IEnumerable<BooksDTO> Books(string search);
        int TotalBooks(string search);
        int GetBookPoint(int bookId);
        void Accept(int id);
        void Reject(int id);
    }
}
