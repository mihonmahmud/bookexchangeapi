﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data
{
    public class BookExchangeDbContext : DbContext
    {
        public BookExchangeDbContext(DbContextOptions<BookExchangeDbContext> options) : base(options)
        {
        }

        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<DeliveryDetail> DeleveryDetails { get; set; }
        public virtual DbSet<DeliveryMan> DeleveryMen { get; set; }
        public virtual DbSet<Login> Logins { get; set; }
        public virtual DbSet<RequestedBook> RequestedBooks { get; set; }
        public virtual DbSet<SellPosting> SellPostings { get; set; }
        public virtual DbSet<User> Users { get; set; }
    }
}
