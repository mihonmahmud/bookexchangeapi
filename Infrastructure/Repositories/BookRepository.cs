﻿using Core.DTOs;
using Core.Entities;
using Core.Interfaces;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Repositories
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        public BookRepository(BookExchangeDbContext context) : base(context) { }

        public IEnumerable<BooksDTO> Books(string search)
        {
            var books = _context.Books as IQueryable<Book>;

            if(search == "accepted")
            {
                books = books.Where(x => x.Status == 1);
            }
            else if(search == "pending")
            {
                books = books.Where(x => x.Status == 0);
            }
            else if (search == "rejected")
            {
                books = books.Where(x => x.Status == 2);
            }

            var booksToReturn = books.Select(x => new BooksDTO()
            {
                BookId = x.BookId,
                BookTitle = x.BookTitle,
                BookAuthor = x.BookAuthor,
                BookEdition = x.BookEdition,
                Point = x.Point,
                UploadedBy = _context.Users.Where(y => y.UserId == x.UserId).Select(z => z.Email).First()
            });

            return booksToReturn;
        }

        public int TotalBooks(string search)
        {
            if (search == "accepted")
                return _context.Books.Count(x => x.Status == 1);
            else if (search == "pending")
                return _context.Books.Count(x => x.Status == 0);
            else if (search == "rejected")
                return _context.Books.Count(x => x.Status == 2);
            else
                return 0;
        }

        public int GetBookPoint(int bookId)
        {
            var b = _context.Books.FirstOrDefault(x => x.BookId == bookId);
            return b.Point;
        }

        public void Accept(int id)
        {
            var b = _context.Books.SingleOrDefault(x => x.BookId == id);

            if (b != null)
            {
                b.Status = 1;
                _context.SaveChanges();
            }
        }

        public void Reject(int id)
        {
            var b = _context.Books.SingleOrDefault(x => x.BookId == id);

            if (b != null)
            {
                b.Status = 2;
                _context.SaveChanges();
            }
        }
    }
}
