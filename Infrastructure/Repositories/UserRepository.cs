﻿using Core.DTOs;
using Core.Entities;
using Core.Interfaces;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Infrastructure.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(BookExchangeDbContext context) : base(context) { }


        public IList<UserDTO> Users()
        {
            var users = _context.Users.Select(p => new UserDTO()
            {
                UserId = p.UserId,
                FullName = p.FullName,
                Email = p.Email,
                Address = p.Address,
                Phone = p.PhoneNo,
                Points = p.Points,
                TotalUpload = _context.Books.Count(x => x.UserId == p.UserId),
                TotalBuy = _context.SellPostings.Count(x => x.BuyerId == p.UserId && x.Status == 4),
                TotalSell = _context.SellPostings.Count(x => x.SellerId == p.UserId && x.Status == 4)
            });

            return users.ToList();
        }

        public bool IsUserExist(string email)
        {
            var user = _context.Logins.SingleOrDefault(x => x.Email == email);

            if (user != null)
                return true;

            return false;
        }

        public UserDTO User(int id)
        {
            var user = _context.Users.Where(x => x.UserId == id).Select(p => new UserDTO()
            {
                UserId = p.UserId,
                FullName = p.FullName,
                Email = p.Email,
                Address = p.Address,
                Phone = p.PhoneNo,
                Points = p.Points,
                TotalUpload = _context.Books.Count(x => x.UserId == p.UserId),
                TotalBuy = _context.SellPostings.Count(x => x.BuyerId == p.UserId && x.Status == 4),
                TotalSell = _context.SellPostings.Count(x => x.SellerId == p.UserId && x.Status == 4)
            }).SingleOrDefault();

            return user;
        }

        public int GetPoint(int id)
        {
            var u = _context.Users.SingleOrDefault(x => x.UserId == id);

            return u.Points;
        }

        public void PutPoint(int id, int point)
        {
            var u = _context.Users.SingleOrDefault(x => x.UserId == id);

            if (u != null)
                u.Points = point;
        }

        public void DeleteByEmail(string mail)
        {
            var li = _context.Logins.SingleOrDefault(x => x.Email == mail && x.Status == 0);
            var us = _context.Users.SingleOrDefault(x => x.Email == mail);
            var books = _context.Books.Where(x => x.UserId == us.UserId).ToList();
            var rbooks = _context.RequestedBooks.Where(x => x.UserId == us.UserId).ToList();
            var buyer = _context.SellPostings.Where(x => x.BuyerId == us.UserId).ToList();
            var seller = _context.SellPostings.Where(x => x.SellerId == us.UserId).ToList();

            _context.Logins.Remove(li);

            if(books != null)
                _context.Books.RemoveRange(books);

            if(rbooks != null)
                _context.RequestedBooks.RemoveRange(rbooks);

            if(buyer != null)
            {
                foreach (var item in buyer)
                {
                    item.BuyerId = -1;
                }
            }
            
            if(seller != null)
            {
                foreach (var item in seller)
                {
                    item.SellerId = -1;
                }
            }
            _context.Users.Remove(us);
        }

        public UserDTO GetByEmail(string mail)
        {
            var user = _context.Users.Where(x => x.Email == mail).Select(p => new UserDTO()
            {
                UserId = p.UserId,
                FullName = p.FullName,
                Email = p.Email,
                Address = p.Address,
                Phone = p.PhoneNo,
                Points = p.Points,
                TotalUpload = _context.Books.Count(x => x.UserId == p.UserId),
                TotalBuy = _context.SellPostings.Count(x => x.BuyerId == p.UserId && x.Status == 4),
                TotalSell = _context.SellPostings.Count(x => x.SellerId == p.UserId && x.Status == 4)
            }).SingleOrDefault();

            return user;
        }

        public int TotalUsers()
        {
            return _context.Users.Count();
        }

        public int GetStatus(string mail)
        {
            var l = _context.Logins.FirstOrDefault(x => x.Email == mail);

            return l.Status;
        }

        public IEnumerable<TopTenDTO> TopTen(string search)
        {
            if (search == "uploader")
                return TopUploader();
            else if (search == "seller")
                return TopSeller();
            else if (search == "buyer")
                return TopBuyer();

            return TopUploader();
        }

        public IEnumerable<TopTenDTO> TopUploader()
        {
            var top = _context.Books.Where(x => x.Status == 1).AsEnumerable().GroupBy(x => x.UserId).OrderByDescending(g => g.Count()).Take(10);

            var data = new List<TopTenDTO>();

            foreach (var item in top)
            {
                int id = Convert.ToInt32(item.Key.ToString());
                var email = _context.Users.Where(x => x.UserId == id).Select(y => y.Email).FirstOrDefault();
                data.Add(new TopTenDTO { Email = email, Count = item.Count() });
            }

            return data.ToList();
        }

        public IEnumerable<TopTenDTO> TopSeller()
        {
            var top = _context.SellPostings.AsEnumerable().GroupBy(x => x.SellerId).OrderByDescending(g => g.Count()).Take(10);

            var data = new List<TopTenDTO>();

            foreach (var item in top)
            {
                int id = Convert.ToInt32(item.Key.ToString());
                var email = _context.Users.Where(x => x.UserId == id).Select(y => y.Email).FirstOrDefault();
                data.Add(new TopTenDTO { Email = email, Count = item.Count() });
            }

            return data.ToList();
        }

        public IEnumerable<TopTenDTO> TopBuyer()
        {
            var top = _context.SellPostings.AsEnumerable().GroupBy(x => x.BuyerId).OrderByDescending(g => g.Count()).Take(10);

            var data = new List<TopTenDTO>();

            foreach (var item in top)
            {
                int id = Convert.ToInt32(item.Key.ToString());
                var email = _context.Users.Where(x => x.UserId == id).Select(y => y.Email).FirstOrDefault();
                data.Add(new TopTenDTO { Email = email, Count = item.Count() });
            }

            return data.ToList();
        }

        public IEnumerable<DeliveryDetail> GetUserDeleveries(int userId)
        {
            IEnumerable<SellPosting> s = _context.SellPostings.Where(x => x.BuyerId == userId && x.Status == 2);
            List<DeliveryDetail> d = new List<DeliveryDetail>();
            foreach (var data in s)
            {
                DeliveryDetail dd = _context.DeleveryDetails.FirstOrDefault(x => x.SellId == data.SellId);

                d.Add(dd);
            }
            return d;
        }

        public IEnumerable<SellPosting> GetbyUserId(int userId)
        {
            return _context.SellPostings.Where(x => x.BuyerId == userId && x.Status == 0);
        }

        public IEnumerable<Book> GetUserAcceptedBooks(int user)
        {
            return _context.Books.Where(x => x.UserId != user && x.Status == 1);
        }

        public IEnumerable<Book> GetUserBooks(int user)
        {
            return _context.Books.Where(x => x.UserId == user).ToList();
        }
    }
}
