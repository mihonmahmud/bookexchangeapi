﻿using Core.DTOs;
using Core.Entities;
using Core.Interfaces;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Repositories
{
    public class DeliveryManRepository : GenericRepository<DeliveryMan>, IDeliveryManRepository
    {
        private readonly Random rand;
        
        public DeliveryManRepository(BookExchangeDbContext context) : base(context)
        {
            rand = new Random();
        }

        public void AddDeliveryMan(DeliveryMan dm)
        {
            int password = rand.Next(1111, 9999);

            Login li = new Login { Email = dm.Email, Password = Convert.ToString(password), Status = 2 };

            _context.DeleveryMen.Add(dm);
            _context.Logins.Add(li);
            _context.SaveChanges();
        }

        public IEnumerable<DeliveryManDTO> AllMen()
        {
            var records = _context.DeleveryMen.OrderByDescending(a => a.DeleveryManId).Select(x => new DeliveryManDTO()
            {
                Id = x.DeleveryManId,
                FullName = x.FullName,
                Email = x.Email,
                Phone = x.Phone,
                Address = x.Address,
                Salary = x.Salary,
                Count = _context.DeleveryDetails.Count(y => y.DeleveryManId == x.DeleveryManId)
            });

            return records.ToList();
        }

        public DeliveryManDTO GetMan(int id)
        {
            var record = _context.DeleveryMen.Where(a => a.DeleveryManId == id).Select(x => new DeliveryManDTO()
            {
                Id = x.DeleveryManId,
                FullName = x.FullName,
                Email = x.Email,
                Phone = x.Phone,
                Address = x.Address,
                Salary = x.Salary,
                Count = _context.DeleveryDetails.Count(y => y.DeleveryManId == x.DeleveryManId)
            }).SingleOrDefault();

            if (record != null)
                return record;

            return null;
        }

        public void DeleteByEmail(string mail)
        {
            var li = _context.Logins.Single(x => x.Email == mail && x.Status == 2);
            var us = _context.DeleveryMen.Single(x => x.Email == mail);

            _context.Logins.Remove(li);
            _context.DeleveryMen.Remove(us);

            _context.SaveChanges();
        }

        public bool IsEmailExist(string mail)
        {
            DeliveryMan record = null;
            User u = null;

            if (_context.DeleveryMen.Any())
                record = _context.DeleveryMen.FirstOrDefault(x => x.Email == mail);

            if (_context.Users.Any())
                u = _context.Users.FirstOrDefault(x => x.Email == mail);

            if (record != null || u != null)
                return true;

            return false;
        }

        public int TotalDeliveryMan()
        {
            return _context.DeleveryMen.Count();
        }

        public int TotalDeliveries()
        {
            return _context.SellPostings.Count(x => x.Status == 4);
        }

        public IEnumerable<DeliveryDetail> GetDeliveriesByID(int dmanID)
        {
            return _context.DeleveryDetails.Where(x => x.DeleveryManId == dmanID).ToList();
        }
    }
}
