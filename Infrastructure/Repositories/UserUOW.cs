﻿using Core.Entities;
using Core.Interfaces;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Repositories
{
    public class UserUOW : IUserUOW
    {
        public IGenericRepository<Login> login { get; private set; }
        public IUserRepository user { get; private set; }
        private readonly BookExchangeDbContext _context;

        public UserUOW(BookExchangeDbContext context, IGenericRepository<Login> li, IUserRepository ur)
        {
            _context = context;
            login = li;
            user = ur;
        }

        public int Complete()
        {
           return _context.SaveChanges();
        }
    }
}
