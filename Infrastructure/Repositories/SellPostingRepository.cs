﻿using Core.DTOs;
using Core.Interfaces;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Repositories
{
    public class SellPostingRepository : ISellPostingRepository
    {
        private readonly BookExchangeDbContext _context;

        public SellPostingRepository(BookExchangeDbContext context)
        {
            _context = context;
        }

        public IEnumerable<DeliveryStatusDTO> DeliveryStatus()
        {
            var records = _context.SellPostings.OrderByDescending(x => x.SellId).Select(x => new DeliveryStatusDTO()
            {
                SellID = x.SellId,
                BookName = _context.Books.Where(y => y.BookId == x.BookId).Select(y => y.BookTitle).FirstOrDefault(),
                SellerName = _context.Users.Where(z => z.UserId == x.SellerId).Select(z => z.FullName).FirstOrDefault(),
                BuyerName = _context.Users.Where(z => z.UserId == x.BuyerId).Select(z => z.FullName).FirstOrDefault(),
                Status = x.Status
            });

            return records.ToList();
        }

        public IEnumerable<DeliveryStatusDTO> PendingList()
        {
            var records = _context.SellPostings.Where(x => x.Status == 1).Select(x => new DeliveryStatusDTO()
            {
                SellID = x.SellId,
                BookName = _context.Books.Where(y => y.BookId == x.BookId).Select(y => y.BookTitle).FirstOrDefault(),
                SellerName = _context.Users.Where(z => z.UserId == x.SellerId).Select(z => z.FullName).FirstOrDefault(),
                BuyerName = _context.Users.Where(z => z.UserId == x.BuyerId).Select(z => z.FullName).FirstOrDefault(),
                Status = x.Status
            });

            return records.ToList();
        }

        public StatusCountDTO StatusCount()
        {
            int accepted = _context.SellPostings.Count(x => x.Status == 1);
            int way = _context.SellPostings.Count(x => x.Status == 3);
            int delivered = _context.SellPostings.Count(x => x.Status == 4);

            return new StatusCountDTO { Accepted = accepted, OnWay = way, Delivered = delivered };
        }
    }
}
