﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.DTOs;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/deliverymen")]
    [ApiController]
    public class DeliveryManController : ControllerBase
    {
        private IDeliveryManRepository _manRepo;
        private IGenericRepository<DeliveryDetail> _details;

        public DeliveryManController(IDeliveryManRepository manRepo, IGenericRepository<DeliveryDetail> details)
        {
            _manRepo = manRepo;
            _details = details;
        }

        [HttpGet("")]
        public ActionResult GetAll()
        {
            return Ok(_manRepo.AllMen().ToList());
        }


        [HttpGet("{id}", Name = "GetDeleveryManById")]
        public ActionResult Get(int id)
        {
            var deliveryMan = _manRepo.GetMan(id);

            if (deliveryMan != null)
                return Ok(deliveryMan);

            return NotFound();            
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] DeliveryMan deliveryMan)
        {
            try
            {

                deliveryMan.DeleveryManId = id;
                _manRepo.Update(deliveryMan);
                return Ok(deliveryMan);

            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("")]
        public ActionResult Post([FromBody] DeliveryMan deliveryMan)
        {
            try
            {
                _manRepo.AddDeliveryMan(deliveryMan);
                string url = Url.Link("GetDeleveryManById", new { id = deliveryMan.DeleveryManId });
                return Created(url, deliveryMan);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var man = _manRepo.Get(id);

            if(man != null)
            {
                _manRepo.DeleteByEmail(man.Email);
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("{id}/deleverydetails")]
        public ActionResult GetDeliveryDtail(int id)
        {
            var delivery = _manRepo.GetDeliveriesByID(id);

            if (delivery != null)
            {
                return Ok(delivery);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost("{id}/deleverydetails/{sid}")]
        public ActionResult Post([FromRoute] int id, [FromRoute] int sid, [FromBody] DeliveryDetail deliveryDetail)
        {
            try
            {
                deliveryDetail.DeleveryManId = id;
                deliveryDetail.SellId = sid;
                deliveryDetail.BookReceivedDate = DateTime.Now;
                deliveryDetail.BookDeleverdDate = DateTime.Now;
                _details.Insert(deliveryDetail);
                string url = Url.Link("GetDeleveryById", new { id = deliveryDetail.DeleveryManId });
                return Created(url, deliveryDetail);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
