﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/total")]
    [ApiController]
    public class TotalController : ControllerBase
    {
        private readonly IBookRepository _bookRepo;
        private readonly IUserRepository _userRepo;
        private readonly IDeliveryManRepository _manRepo;

        public TotalController(IBookRepository bookRepo, IUserRepository userRepo, IDeliveryManRepository manRepo)
        {
            _bookRepo = bookRepo;
            _userRepo = userRepo;
            _manRepo = manRepo;
        }

        [HttpGet("")]
        public ActionResult Get(string search)
        {
            if(search == null)
            {
                return NoContent();
            }
            else
            {
                if (search.ToLower() == "user")
                {
                    return Ok(_userRepo.TotalUsers());
                }
                else if (search.ToLower() == "deliveryman")
                {
                    return Ok(_manRepo.TotalDeliveryMan());
                }
                else if (search.ToLower() == "deliveries")
                {
                    return Ok(_manRepo.TotalDeliveries());
                }
                else
                {
                    return Ok(_bookRepo.TotalBooks(search.ToLower()));
                }
            }           
        }
    }
}
