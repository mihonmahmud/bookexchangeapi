﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.DTOs;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserUOW _userUOW;

        public UserController(IUserUOW userUOW)
        {
            _userUOW = userUOW;
        }

        [HttpGet("")]
        public ActionResult<UserDTO> Get()
        {
            var users = _userUOW.user.Users();
            List<UserDTO> u = new List<UserDTO>();

            foreach(var data in users)
            {
                data.Links.Add(new Links() { HRef = "{URL}/api/users", Method = "GET", Rel = "Get all the user list" });
                data.Links.Add(new Links() { HRef = "{URL}/api/users", Method = "POST", Rel = "Create a new user resource" });
                data.Links.Add(new Links() { HRef = "{URL}/api/users/" + data.UserId, Method = "PUT", Rel = "Modify an existing user resource" });
                data.Links.Add(new Links() { HRef = "{URL}/api/users/" + data.UserId, Method = "DELETE", Rel = "Delete an existing user resource" });

                u.Add(data);
            }

            return Ok(u.ToList());
        }

        [HttpGet("{id}", Name = "GetUserById")]
        public ActionResult Get(int id)
        {
            var us = _userUOW.user.User(id);

            if (us != null)
            {
                us.Links.Add(new Links() { HRef = "http://localhost:62832/api/users", Method = "GET", Rel = "Get all the user list" });
                us.Links.Add(new Links() { HRef = "http://localhost:62832/api/users", Method = "POST", Rel = "Create a new user resource" });
                us.Links.Add(new Links() { HRef = "http://localhost:62832/api/users/" + us.UserId, Method = "PUT", Rel = "Modify an existing user resource" });
                us.Links.Add(new Links() { HRef = "http://localhost:62832/api/users/" + us.UserId, Method = "DELETE", Rel = "Delete an existing user resource" });

                return Ok(us);
            }
            else
            {
                return NotFound();
            }

        }

        [HttpPost("")]
        public ActionResult Post([FromBody] UserDataDTO user)
        {
            if(_userUOW.user.IsUserExist(user.Email) == false)
            {
                User u = new User
                {
                    FullName = user.FullName,
                    Email = user.Email,
                    Address = user.Address,
                    PhoneNo = user.Phone,
                    Points = 50
                };

                Login l = new Login
                {
                    Email = user.Email,
                    Password = user.Password,
                    Status = 0
                };

                try
                {
                    _userUOW.user.Insert(u);
                    _userUOW.login.Insert(l);
                    _userUOW.Complete();

                    return Ok(u);
                }
                catch (Exception)
                {
                    return StatusCode(500);
                }
            }
            else
            {
                return Conflict();
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] User user)
        {
            try
            {
                user.UserId = id;
                _userUOW.user.Update(user);
                return Ok(user);

            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (_userUOW.user.Get(id) != null)
            {
                _userUOW.user.Delete(id);
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("{id}/point")]
        public ActionResult GetPoint(int id)
        {
            var us = _userUOW.user.GetPoint(id);

            return Ok(us);
        }

        [HttpPost("{id}/point")]
        public ActionResult PutPoint(int id, [FromBody] IdDTO idt)
        {
            if (idt.Ids != null)
            {
                foreach (var item in idt.Ids)
                {
                    _userUOW.user.PutPoint(id, item);
                    _userUOW.Complete();
                }
                return Ok();
            }

            return NoContent();
        }

        [HttpGet("{userId}/deliveries")]
        public ActionResult UserDeleveries(int userId)
        {
            var deliveries = _userUOW.user.GetUserDeleveries(userId);
            if (deliveries != null)
            {
                return Ok(deliveries);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("{userId}/sellposting", Name = "GetSellPostingUserId")]
        public ActionResult GetbyUserId(int userId)
        {
            var sellPosting = _userUOW.user.GetbyUserId(userId);

            if (sellPosting != null)
            {
                return Ok(sellPosting);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("{userId}/acceptedbooks")]
        public ActionResult GetAcceptedBooks(int userId)
        {
            var books = _userUOW.user.GetUserAcceptedBooks(userId);

            if (books != null)
            {
                return Ok(books);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("{userId}/books")]
        public ActionResult GetBooks(int userId)
        {
            var books = _userUOW.user.GetUserBooks(userId);

            if (books != null)
            {
                return Ok(books);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
