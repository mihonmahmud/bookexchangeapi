﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SellPostingController : ControllerBase
    {
        private readonly IGenericRepository<SellPosting> _repo;

        public SellPostingController(IGenericRepository<SellPosting> repo)
        {
            _repo = repo;
        }

        [HttpGet("")]
        public ActionResult GetAll()
        {
            return Ok(_repo.GetAll());
        }


        [Route("{id}", Name = "GetSellPostingId")]
        public ActionResult Get(int id)
        {
            var sellPosting = _repo.Get(id);

            if (sellPosting != null)
            {
                return Ok(sellPosting);
            }
            else
            {
                return NotFound();
            }

        }

        [Route("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] SellPosting sellPosting)
        {
            try
            {

                sellPosting.SellId = id;
                _repo.Update(sellPosting);
                return Ok(sellPosting);

            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("")]
        public ActionResult Post([FromBody] SellPosting sellPosting)
        {
            try
            {
                _repo.Insert(sellPosting);
                string url = Url.Link("GetSellPostingId", new { id = sellPosting.SellId });
                return Created(url, sellPosting);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [Route("{id}")]
        public ActionResult Delete(int id)
        {
            if (_repo.Get(id) != null)
            {
                _repo.Delete(id);
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
