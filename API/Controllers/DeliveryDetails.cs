﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveryDetails : ControllerBase
    {
        private IGenericRepository<DeliveryDetail> _repo;

        public DeliveryDetails(IGenericRepository<DeliveryDetail> repo)
        {
            _repo = repo;
        }

        [HttpGet("")]
        public ActionResult GetAll()
        {
            return Ok(_repo.GetAll());
        }


        [HttpGet("{id}", Name = "GetDeleveryById")]
        public ActionResult Get(int id)
        {
            var delivery = _repo.Get(id);

            if (delivery != null)
            {
                return Ok(delivery);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] DeliveryDetail deliveryDetail)
        {
            try
            {

                deliveryDetail.DeleveryId = id;
                _repo.Update(deliveryDetail);
                return Ok(deliveryDetail);

            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("")]
        public ActionResult Post([FromBody] DeliveryDetail deliveryDetail)
        {
            try
            {
                _repo.Insert(deliveryDetail);
                string url = Url.Link("GetDeleveryById", new { id = deliveryDetail.DeleveryManId });
                return Created(url, deliveryDetail);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (_repo.Get(id) != null)
            {
                _repo.Delete(id);
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
