﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TopController : ControllerBase
    {
        private readonly IUserUOW _userUOW;

        public TopController(IUserUOW userUOW)
        {
            _userUOW = userUOW;
        }

        [HttpGet("")]
        public ActionResult TopUploader(string search)
        {
            if(search == null)
            {
                return NoContent();
            }
            else
            {
                return Ok(_userUOW.user.TopTen(search));
            }
        }
    }
}
