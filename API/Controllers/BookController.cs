﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Core.DTOs;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookRepository _repo;

        public BookController(IBookRepository repo)
        {
            _repo = repo;
        }

        [HttpGet("books")]
        public ActionResult GetAll(string search)
        {
            return Ok(_repo.Books(search));
        }

        [HttpGet("books/{id}")]
        public ActionResult Get(int id)
        {
            var book = _repo.Get(id);

            if (book == null)
                return NotFound();

            return Ok(book);
        }

        [HttpPost("books")]
        public ActionResult Post([FromBody] Book book)
        {
            try
            {
                _repo.Insert(book);

                string url = Url.Link("GetBookById", new { id = book.BookId });
                return Created(url, book);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPut("books/{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] Book book)
        {
            try
            {
                book.BookId = id;
                _repo.Update(book);
                return Ok(book);
            }
            catch(Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("books/{id}")]
        public ActionResult Delete(int id)
        {
            if (_repo.Get(id) != null)
            {
                _repo.Delete(id);
                return NoContent();
            }
            else
                return NotFound();
        }

        [HttpGet("books/{bookId}/point", Name = "GetBookspoint")]
        public ActionResult GetBooksPoint(int bookId)
        {
            int? booksPoint = _repo.GetBookPoint(bookId);

            if (booksPoint != null)
            {
                return Ok(booksPoint);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost("acceptbooks")]
        public ActionResult PendingBooks([FromBody] IdDTO idt)
        {
            if (idt.Ids != null)
            {
                foreach (var item in idt.Ids)
                {
                    _repo.Accept(item);
                }

                return Ok();
            }

            return NoContent();
        }

        [HttpPost("rejectbooks")]
        public ActionResult RejectBooks([FromBody] IdDTO idt)
        {
            if (idt.Ids != null)
            {
                foreach (var item in idt.Ids)
                {
                    _repo.Reject(item);
                }

                return Ok();
            }

            return NoContent();
        }
    }
}
