﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IUserUOW _userUOW;

        public LoginController(IUserUOW userUOW)
        {
            _userUOW = userUOW;
        }

        [HttpGet("{mail}")]
        public ActionResult GetStatus(string mail)
        {
            try
            {
                int us = _userUOW.user.GetStatus(mail);

                return Ok(us);
            }
            catch (Exception)
            {
                return NoContent();
            }
        }
    }
}
