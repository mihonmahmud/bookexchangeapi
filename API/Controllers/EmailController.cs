﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IUserUOW _userUOW;

        public EmailController(IUserUOW userUOW)
        {
            _userUOW = userUOW;
        }

        [HttpGet("{email}")]
        public ActionResult Get(string email)
        {
            var us = _userUOW.user.GetByEmail(email);

            if (us != null)
            {
                return Ok(us);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete("{email}")]
        public ActionResult DeleteByEmail(string email)
        {
            try
            {
                if(_userUOW.user.IsUserExist(email) == true)
                {
                    _userUOW.user.DeleteByEmail(email);
                    _userUOW.Complete();
                    return NoContent();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception) 
            {
                return StatusCode(500);
            }
        }
    }
}
