﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api")]
    [ApiController]
    public class DeliveryStatusController : ControllerBase
    {
        private ISellPostingRepository _repo;

        public DeliveryStatusController(ISellPostingRepository repo)
        {
            _repo = repo;
        }

        [HttpGet("status")]
        public ActionResult GetDeliveryStatus()
        {
            return Ok(_repo.DeliveryStatus());
        }

        [HttpGet("pending")]
        public ActionResult GetPendingList()
        {
            return Ok(_repo.PendingList());
        }

        [HttpGet("deliverycount")]
        public ActionResult GetDeliveryCount()
        {
            return Ok(_repo.StatusCount());
        }
    }
}
